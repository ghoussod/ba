## Installation and How To Run

Clone the repository:

```sh
$ git clone https://gitlab.com/ghoussod/ba.git
$ cd ba/djangoProject
```

Create a virtual environment to install dependencies then activate it:

```sh
$ python3 -m venv env
$ source env/bin/activate
```

Install the dependencies in the env:

```sh
(env)$ pip install -r requirements.txt
```
Note the `(env)` in front of the prompt (indication of being in the env). 

Once `pip` has finished downloading the dependencies:
```sh
(env)$ python manage.py runserver
```
And navigate to `http://localhost:8000/admin/`.

Login with

Username: Admin
PW: bachelor2021
