"""djangoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from clustering import views

#for api
router = routers.DefaultRouter()
router.register(r'db', views.dbView, 'clustering')
router.register(r'existingGrocery',views.existingGroceryView)

#add urls to application
app_name = 'clustering'
urlpatterns = [
    path('clustering/', include('clustering.urls')),
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),


]

from clustering.urls import accounts_urlpatterns

urlpatterns += accounts_urlpatterns  # add URLs for authentication
