import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.models import Group

from .serializers import dbSerializer, existingGrocerySerializer
from .models import db, Grocery, Recycling, Post, Station, ExistingGrocery
import numpy as np
import geopy.distance
"""
Fuzzy Logic Toolbox for Python.
"""
import skfuzzy as fuzz

"""
view used to store markers from map in frontend
"""


class dbView(viewsets.ModelViewSet):
    serializer_class = dbSerializer
    queryset = db.objects.all()


"""
view used to store markers from map in frontend
"""


class existingGroceryView(viewsets.ModelViewSet):
    serializer_class = existingGrocerySerializer
    queryset = ExistingGrocery.objects.all()

"""
calculate the distance between two points with lat and long
"""

def calculateDistance(point1, point2):
    coords_1 = (point1[0], point1[1])
    coords_2 = (point2[0], point2[1])
    kilometers = geopy.distance.geodesic(coords_1, coords_2).km
    return kilometers

"""
used in clusters function
"""

number = 0
def doFuzzyClustering(points_x, points_y, T):
    points = np.array([points_x, points_y])

    global number
    if number>=3:
        number= 0
    else:
        number= number+1

    # delete everything in db
    T.objects.all().delete()

    # fcm = FCM(n_clusters=2)
    allCntr = []
    fpcs =[]

    #fpc for 1 cluster is always 1, so start with a number of clusters of 2  until all points
    for ncntrs in range(2, len(points_x) + 1):
        cntr, u, u0, d, jm, p, fpc = fuzz.cluster.cmeans(points, c=ncntrs, m=35, error=0.005, maxiter=1000, init=None) #or init=None
        # Store fpc values for later
        fpcs.append(fpc)
        # store centers with their fpc in array
        if allCntr == []:
            allCntr.append(cntr.tolist())
            allCntr.append(fpc.tolist())
        #if the fpc of the last calculated centers is smaller, then replace them by current centers
        elif allCntr[-1] <= fpc:

            while len(allCntr) > 0:
                allCntr.pop()
            allCntr.append(cntr.tolist())
            allCntr.append(fpc.tolist())

        else:
            print("")



    #create centers in db, if two centroids too near take only one (because fpc method cannot be used for one centroid, so it might draw two centroids at the same position)
    for center in allCntr[0]:
        if(len(allCntr[0])==2):
            #if two centers closer than 0.1km, then take only one center in db
            if(calculateDistance(allCntr[0][0], allCntr[0][1])<0.1):
                T.objects.create(name=1, centroid_x=center[0], centroid_y=center[1], fpc=1)
                break
        T.objects.create(name=len(allCntr[0]), centroid_x=center[0], centroid_y=center[1], fpc=1)
    import matplotlib.pyplot as plt

    #plot the fpc for the four services
    fig, ax2 = plt.subplots()
    ax2.plot(np.r_[2:len(fpcs)+2], fpcs)
    ax2.set_xlabel("Number of centers")
    ax2.set_title(str(T))
    ax2.set_ylabel("Fuzzy partition coefficient")
    fig.savefig('./plot'+str(number))   # save the figure to file
    plt.close(fig)    # close the figure window


"""
calculate cluster centroids with algorithm when someone requests them
"""

@api_view(['GET'])
def clusters(request):
    """
    calculate centroids for all data points from the users
    """
    # Grocery centroids
    for item in [Grocery, Recycling, Post, Station]:
        points_lat = np.array([])
        points_lng = np.array([])
        for groceryItem in db.objects.all():

            if item == Grocery:
                if groceryItem.lat_0 != -100.000000:
                    points_lat = np.append(points_lat, float(groceryItem.lat_0))
                    points_lng = np.append(points_lng, float(groceryItem.lng_0))
            elif item == Recycling:
                if groceryItem.lat_1 != -100.000000:
                    points_lat = np.append(points_lat, float(groceryItem.lat_1))
                    points_lng = np.append(points_lng, float(groceryItem.lng_1))
            elif item == Post:
                if groceryItem.lat_2 != -100.000000:
                    points_lat = np.append(points_lat, float(groceryItem.lat_2))
                    points_lng = np.append(points_lng, float(groceryItem.lng_2))
            elif item == Station:
                if groceryItem.lat_3 != -100.000000:
                    points_lat = np.append(points_lat, float(groceryItem.lat_3))
                    points_lng = np.append(points_lng, float(groceryItem.lng_3))
            else:
                ""
        doFuzzyClustering(points_lat, points_lng, item)


    """
    get clusters for each
    """
    grocery_array = []
    for centroid in Grocery.objects.all():
        nbOfCntrs = centroid.name
        lat = centroid.centroid_x
        lng = centroid.centroid_y
        fpc = centroid.fpc
        grocery_array.append({"nbOfCntrs": nbOfCntrs, "lat": lat, "lng": lng, "fpc": fpc})

    grocery_centroids = grocery_array

    recycling_array = []
    for centroid in Recycling.objects.all():
        nbOfCntrs = centroid.name
        lat = centroid.centroid_x
        lng = centroid.centroid_y
        fpc = centroid.fpc
        recycling_array.append({"nbOfCntrs": nbOfCntrs, "lat": lat, "lng": lng, "fpc": fpc})
    recycling_centroids = recycling_array

    post_array = []
    for centroid in Post.objects.all():
        nbOfCntrs = centroid.name
        lat = centroid.centroid_x
        lng = centroid.centroid_y
        fpc = centroid.fpc
        post_array.append({"nbOfCntrs": nbOfCntrs, "lat": lat, "lng": lng, "fpc": fpc})
    post_centroids = post_array

    station_array = []
    for centroid in Station.objects.all():
        nbOfCntrs = centroid.name
        lat = centroid.centroid_x
        lng = centroid.centroid_y
        fpc = centroid.fpc
        station_array.append({"nbOfCntrs": nbOfCntrs, "lat": lat, "lng": lng, "fpc": fpc})
    station_centroids = station_array

    return Response({"Grocery": grocery_centroids, "Recycling": recycling_centroids, "Post": post_centroids,
                     "Station": station_centroids})


'''
Get the group of the user
'''

@api_view(['GET'])
def groupMembership(request):
    users_in_user = Group.objects.get(name="user").user_set.all()
    users_in_admin = Group.objects.get(name="admin").user_set.all()
    users_in_expert = Group.objects.get(name="expert").user_set.all()

    if request.user in users_in_admin:
        return Response("admin")
    elif request.user in users_in_user:
        return Response("user")
    elif request.user in users_in_expert:
        return Response("expert")
    else:
        return Response("no group")


""" 
get own datapoints of each service (data points of one user)
"""

@api_view(['GET'])
def getOwnData(request):
    try:
        data = db.objects.get(pk=request.GET.get('name', -1))
    except db.DoesNotExist:
        return Response("User Not Found")

    serializer = dbSerializer(data)
    return Response(serializer.data)


"""
get all datapoints from all users and services
"""

@api_view(['GET'])
def allPoints(request):
    allObjects = db.objects.all().values_list()
    return Response(allObjects)
