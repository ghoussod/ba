from django.urls import path
from django.conf.urls import url, include

from .views import groupMembership, clusters, allPoints, existingGroceryView, getOwnData

#to have 127.0.0.1:8000/clustering/group
urlpatterns = [
    path('group/', groupMembership),
    path('clusters/',clusters),
    path('allPoints/',allPoints),
    path('getOwnData/',getOwnData),
    path('existinggrocery/',existingGroceryView)
]

#to have 127.0.0.1:8000/api
#djoser used for authentication: generates token
accounts_urlpatterns = [
    url(r'api/v1/', include('djoser.urls')),
    url(r'api/v1/', include('djoser.urls.authtoken')),
]
