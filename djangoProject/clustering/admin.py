# # Register your models here.
from django.contrib import admin

from .models import db, Grocery, Recycling, Post, Station, ExistingGrocery
from import_export.admin import ImportExportModelAdmin

"""
To show all lists of data in localhost:8000/admin
"""

class ExistingGroceryAdmin(admin.ModelAdmin):
    list_display = ('name', 'lat_0', 'lng_0')


class dbAdmin(admin.ModelAdmin):
    list_display = ('name', 'lat_0', 'lng_0','lat_1' ,'lng_1' ,'lat_2' ,'lng_2','lat_3' ,'lng_3')

class ClusterAdmin(admin.ModelAdmin):
    model = Grocery
    list_display = ('name', 'centroid_x', 'centroid_y', 'fpc')

class ClusterAdmin2(admin.ModelAdmin):
    model = Recycling
    list_display = ('name', 'centroid_x', 'centroid_y', 'fpc')

class ClusterAdmin3(admin.ModelAdmin):
    model = Post
    list_display = ('name', 'centroid_x', 'centroid_y', 'fpc')

class ClusterAdmin4(admin.ModelAdmin):
    model = Station
    list_display = ('name', 'centroid_x', 'centroid_y', 'fpc')


admin.site.register(db, dbAdmin)
admin.site.register(Grocery, ClusterAdmin)
admin.site.register(Recycling, ClusterAdmin2)
admin.site.register(Post, ClusterAdmin3)
admin.site.register(Station, ClusterAdmin4)
admin.site.register(ExistingGrocery, ExistingGroceryAdmin)


