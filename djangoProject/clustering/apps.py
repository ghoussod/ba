from django.apps import AppConfig


class ClusteringConfig(AppConfig):
    name = 'clustering'
    default_auto_field = 'django.db.models.BigAutoField'
