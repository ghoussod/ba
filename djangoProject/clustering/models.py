from django.db import models

# Create your models here.
from django.db import models

#model to store each datapoint of a service from one user
class db(models.Model):
    name = models.CharField(primary_key=True, max_length=200)
    lat_0 = models.DecimalField(max_digits=9, decimal_places=6)
    lng_0 = models.DecimalField(max_digits=9, decimal_places=6)
    lat_1 = models.DecimalField(max_digits=9, decimal_places=6)
    lng_1 = models.DecimalField(max_digits=9, decimal_places=6)
    lat_2 = models.DecimalField(max_digits=9, decimal_places=6)
    lng_2 = models.DecimalField(max_digits=9, decimal_places=6)
    lat_3 = models.DecimalField(max_digits=9, decimal_places=6)
    lng_3 = models.DecimalField(max_digits=9, decimal_places=6)
    def __str__(self):
        return self.name

#model for all cluster centers for grocery service
class Grocery(models.Model):
    name = models.CharField(max_length=100, default='')
    centroid_x = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    centroid_y = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    fpc = models.DecimalField(max_digits=9, decimal_places=8, default=0)
    def __str__(self):
        return self.name

#model for all cluster centers for recycling service
class Recycling(models.Model):
    name = models.CharField(max_length=100, default='')
    centroid_x = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    centroid_y = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    fpc = models.DecimalField(max_digits=9, decimal_places=8, default=0)
    def __str__(self):
        return self.name

#model for all cluster centers for post service
class Post(models.Model):
    name = models.CharField(max_length=100, default='')
    centroid_x = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    centroid_y = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    fpc = models.DecimalField(max_digits=9, decimal_places=8, default=0)
    def __str__(self):
        return self.name

#model for all cluster centers for station service
class Station(models.Model):
    name = models.CharField(max_length=100, default='')
    centroid_x = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    centroid_y = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    fpc = models.DecimalField(max_digits=9, decimal_places=8, default=0)
    def __str__(self):
        return self.name


#model for all datapoints from existing grocery stores uploaded with excel
class ExistingGrocery(models.Model):
    name = models.CharField(primary_key=True, max_length=200)
    lat_0 = models.DecimalField(max_digits=9, decimal_places=6)
    lng_0 = models.DecimalField(max_digits=9, decimal_places=6)
    def __str__(self):
        return self.name

