from rest_framework import serializers
from .models import db, ExistingGrocery


#serializer used for getting data from frontend
class dbSerializer(serializers.ModelSerializer):
    class Meta:
        model = db
        fields = ('name', 'lat_0', 'lng_0','lat_1' ,'lng_1' ,'lat_2' ,'lng_2','lat_3' ,'lng_3')

class existingGrocerySerializer(serializers.ModelSerializer):
    class Meta:
        model = ExistingGrocery
        fields =  ('name', 'lat_0', 'lng_0')


